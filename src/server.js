var express = require('express');
var app = express();
var server = app.listen(process.env.PORT || 8089);
var io = require('socket.io').listen(server);

app.use(express.static(__dirname + '/client/'));

var calculations = []
io.sockets.on('connection', function (client) {


	console.log("Connection established")
	var _lastTenCalculations = function() {
		console.log("Sending last 10 calculations.")
		var latestCalculations = [];
		var calculationsCount = 0;
		console.log("calculations.length ", calculations.length);
		for (var i= calculations.length - 1; i >= 0 && calculations.length > 0; i--) {
			if(calculationsCount <= 10) {
				calculationsCount++;
				latestCalculations.push(calculations[i]);
			} else {
				break;
			}
		}
		console.log("Passing last 10 calculations ", latestCalculations)
		client.emit("calculationsList", latestCalculations);
	}

	_lastTenCalculations();

    client.on('calculationToServer', function (calculation) {
		console.log("Recieved a calculation. ", calculation);
		calculation.id = Date.now();
		calculations.push(calculation);
		console.log('Adding new calculation', calculation);
		console.log('Calculations length', calculations.length);
        client.broadcast.emit("calculationToClient", calculation);
    });
});

