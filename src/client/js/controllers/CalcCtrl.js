angular.module('Calculation')
.controller("CalcCtrl", [
			'$scope'
			, '$location'
			, '$timeout'
			, '$q'
			, 'Socket'
			, '$mdSidenav'
			, '$mdMedia'
			, '$mdDialog'
			, function(
				$scope
				, $location
				, $timeout
				, $q
				, Socket
				, $mdSidenav
				, $mdMedia
				, $mdDialog){

					
					$scope.$mdMedia = $mdMedia;
					var initializing = true
					$scope.calcOptions = [
						{
							'val': 1,
							'symbol': '+'
						},
						{
							'val': 2,
							'symbol': '-'
						},
						{
							'val': 3,
							'symbol': '*'
						},
						{
							'val': 4,
							'symbol': '/'
						},
					];

					$scope.calculation = {
						'number1': 2
						, 'number2': 3
						, 'operation': $scope.calcOptions[0]
						, 'result': 5}

					var calculate = function() {
						var defered = $q.defer()
						if($scope.calculation.number1 == undefined || $scope.calculation.number2 == undefined) {
							return;
						}
						switch($scope.calculation.operation.val) {
							case 1: 
								defered.resolve($scope.calculation.number1 + $scope.calculation.number2);
								break;
							case 2: 
								defered.resolve($scope.calculation.number1 - $scope.calculation.number2);
								break;
							case 3: 
								defered.resolve($scope.calculation.number1 * $scope.calculation.number2);
								break;
							case 4: 
								if($scope.calculation.number2 == 0) {
									defered.reject();
									break;
								}
								defered.resolve($scope.calculation.number1 / $scope.calculation.number2);
								break;
						}
						return defered.promise;
					}

					var verifyAndBroadCast = function(newValue) {
						if (initializing) {
							$timeout(function() { 
								initializing = false; 
							});
						} else {
							if(newValue == undefined) {
								return;
							}
							$scope.broadcastCalculation();
						}
					}

					$scope.$watch('calculation.number1', function(newValue) {
						verifyAndBroadCast(newValue);
					});

					$scope.$watch('calculation.number2', function(newValue) {
						verifyAndBroadCast(newValue);
					});

					$scope.$watch('calculation.operation', function(newValue) {
						verifyAndBroadCast(newValue);
					});

					$scope.broadcastCalculation = function() {
						var promise = calculate();
						var newCalculation;
						promise.then(function(result) {
							$scope.calculation.result = result;
							Socket.emit("calculationToServer", $scope.calculation);
							$scope.calculation.id = Date.now();
							newCalculation = angular.copy($scope.calculation);
							newCalculation.mine = true;
							$scope.calculations.push(newCalculation);
						}, function() {
							$mdDialog.show({
								template:
								'<md-dialog>' +
								'	<md-toolbar class="md-accent">' + 
								'		<div class="md-toolbar-tools"><h2>Error</h2></div>' +
								'	</md-toolbar>' +
								'	<md-dialog-content>' +
								'		<md-content layout-padding>' +
								'		I don\'t know what to say if you divide by zero, Please watch <b>\'The Man Who Knew Infinity\'</b> for more info.</md-dialog-content>' +
								'		</md-content>' +
								'	</md-dialog-content>' +
								'	<md-dialog-actions>' +
								'		<md-button ng-click="closeDialog()" class="md-primary">' +
								'			Ok' +
								'		</md-button>' +
								'	</md-dialog-actions>' +
								'</md-dialog>',
								controller: function DialogController($scope, $mdDialog) {
									$scope.closeDialog = function() {
										$mdDialog.hide();
									}
								}
							});
						}, function() {
						});
					}


					Socket.connect($location.absUrl());

					Socket.on("calculationsList", function(calculations) {
						$scope.calculations = calculations;
					});

					Socket.on("calculationToClient", function(calculation) {
						if($scope.calculations.length == 10) {
							$scope.calculations.splice(0, 1);
						}
						$scope.calculations.push(calculation);
					});

					$scope.showSideBar = function() {
						$mdSidenav('left').open();
					}
					
					$scope.hideSideBar = function() {
						$mdSidenav('left').close();
					}
}]);
