```html
How to install:
1. Download source code.
2. Hope you have node and npm, then in root directory run "npm install", that installs all dependencies.
3. Then from the same place run "node src/server.js", thats it your server is up.

Open up your browser and hit http://localhost:8089/


Description:
A calculator website which logs calculations as they happen and shares those calculations with everyone connected to the website. 
For example, user A and user B go to your site at the same time. User A calculates "5 + 5", which equals "10". This is logged below the calculator as "5+5 = 10". 
User B is updated about this calculation right after user A posts it. Now user B calculates "3*4". This calcs to 12 and displays "3*4=12" right below the prior calculation. 
User A sees this update immediately after user B posts it. Results remains between sessions. 
Last 10 calculations are shown from most recent to oldest.
```
